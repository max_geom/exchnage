package exchange

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
)

// DataInterface - интрефейс данных
type DataInterface interface {
	getResponseJson() <-chan CleanedData
}

// getDataBodyFromApi - получение данных из url
func getDataBodyFromApi(url string) (*http.Response, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, errors.New("Ошибка запроса: Получить данных не удалось!")
	}

	return response, nil
}

// showBaseError - простой вывод и обработка ошибки
func showBaseError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

// floatFastParse - преобразование строки в число с типов float64
func floatFastParse(s string) float64 {
	res, _ := strconv.ParseFloat(s, 64)
	return res
}

// PoloniexTradeResult - сделка биржи poloniex
type PoloniexTradeResult struct {
	ID     int    `json:"globalTradeID"` // id
	Date   string `json:"date"`          // дата
	Price  string `json:"rate"`          // цена
	Amount string `json:"amount"`        // количчество
}

// PoloniexResponseTrade - торги биржи poloniex
type PoloniexResponseTrade []PoloniexTradeResult

// PoloniexResponseTrade - сделка на бирже binance
type BinanceTradeResult struct {
	ID     int    `json:"id"`    // id
	Price  string `json:"price"` // цена
	Amount string `json:"qty"`   // количество
	Date   int    `json:"time"`  // дата
}

// BinanceResponseTrade - торги на бирже binance
type BinanceResponseTrade []BinanceTradeResult

// CleanedData - результирующие данные
type CleanedData struct {
	ID     int
	Date   string
	Price  float64
	Amount float64
}

type ExchangeCleanedData []CleanedData

// getResponseJson - получение данных с биржи poloniex чепез api
func (poloniex *PoloniexResponseTrade) getResponseJson() <-chan CleanedData {
	out := make(chan CleanedData)
	response, err := getDataBodyFromApi("https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_ETH")
	showBaseError(err)
	defer response.Body.Close()
	json.NewDecoder(response.Body).Decode(poloniex)
	go func() {
		defer close(out)
		for _, item := range *poloniex {
			fmt.Println(item)
			out <- CleanedData{
				ID:     item.ID,
				Date:   item.Date,
				Price:  floatFastParse(item.Price),
				Amount: floatFastParse(item.Amount),
			}
		}
	}()
	return out
}

// getResponseJson - получение данных с биржи binance чепез api
func (binance *BinanceResponseTrade) getResponseJson() <-chan CleanedData {
	out := make(chan CleanedData)
	response, err := getDataBodyFromApi("https://api.binance.com/api/v1/trades?symbol=ETHBTC")
	showBaseError(err)
	defer response.Body.Close()
	json.NewDecoder(response.Body).Decode(binance)
	go func() {
		defer close(out)
		for _, item := range *binance {
			out <- CleanedData{
				ID:     item.ID,
				Date:   strconv.Itoa(item.Date),
				Price:  floatFastParse(item.Price),
				Amount: floatFastParse(item.Amount),
			}
		}
	}()

	return out
}

// generateChan - создание канала для данных с 2х бирж
func generateChan(dataInterface []DataInterface) <-chan DataInterface {
	out := make(chan DataInterface)
	go func() {
		defer close(out)
		for _, exchange := range dataInterface {
			out <- exchange
		}
	}()
	return out
}

// getCleanedData - формирование конала с итоговыми данными
func getCleanedData(in <-chan DataInterface) chan CleanedData {
	out := make(chan CleanedData)
	go func() {
		defer close(out)
		for cd := range in {
			for item := range cd.getResponseJson() {
				out <- item
			}
		}
	}()
	return out
}

// ShowResults - вывод результата запроса
func (ecd *ExchangeCleanedData) ShowResults() {
	for _, item := range *ecd {
		fmt.Println(item)
	}
}

// GetResultFromExchange - торги с бирж binance poloniex
func GetResultFromExchange() (cdResult ExchangeCleanedData) {
	exchanges := generateChan([]DataInterface{new(BinanceResponseTrade), new(PoloniexResponseTrade)})
	for cleanedData := range getCleanedData(exchanges) {
		cdResult = append(cdResult, cleanedData)
	}
	return
}
